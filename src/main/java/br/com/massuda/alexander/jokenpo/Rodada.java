/**
 * 
 */
package br.com.massuda.alexander.jokenpo;

import java.util.List;

import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;

/**
 * @author Alex
 *
 */
public class Rodada extends EntidadeModelo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8449061039740517527L;
	
	private List<Usuario> usuario;
	private List<Movimento> movimentos;
	private Usuario ganhador;

	public List<Usuario> getUsuario() {
		return usuario;
	}
	public void setUsuario(List<Usuario> usuario) {
		this.usuario = usuario;
	}
	public List<Movimento> getMovimentos() {
		return movimentos;
	}
	public void setMovimentos(List<Movimento> movimentos) {
		this.movimentos = movimentos;
	}
	public Usuario getGanhador() {
		return ganhador;
	}
	public void setGanhador(Usuario ganhador) {
		this.ganhador = ganhador;
	}


}
