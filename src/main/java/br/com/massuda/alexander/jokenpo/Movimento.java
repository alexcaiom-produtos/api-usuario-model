/**
 * 
 */
package br.com.massuda.alexander.jokenpo;

/**
 * @author Alex
 *
 */
public class Movimento {

	private Rodada rodada;

	public Rodada getRodada() {
		return rodada;
	}

	public void setRodada(Rodada rodada) {
		this.rodada = rodada;
	}
	
}
