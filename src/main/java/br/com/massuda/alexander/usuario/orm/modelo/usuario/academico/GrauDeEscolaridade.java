package br.com.massuda.alexander.usuario.orm.modelo.usuario.academico;

public enum GrauDeEscolaridade {

	PRIMARIO,
	ENSINO_FUNDAMENTAL_I,
	ENSINO_FUNDAMENTAL_II,
	ENSINO_MEDIO,
	ENSINO_TECNICO,
	ENSINO_SUPERIOR,
	POS_GRADUACAO,
	MESTRADO,
	DOUTORADO,
	POS_DOUTORADO
	
}
