package br.com.massuda.alexander.usuario.orm.modelo;

import java.util.Calendar;

import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Tabela;
import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Transiente;
import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;

@Tabela(nome = "tbl_usuario_foto")
public class Foto extends EntidadeModelo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3364587453930847334L;
	private String descricao;
	private String caminho;
	private Calendar data;
	@Transiente
	private Usuario usuario;
	@Transiente
	private String base64;
	
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getCaminho() {
		return caminho;
	}
	public void setCaminho(String caminho) {
		this.caminho = caminho;
	}
	public Calendar getData() {
		return data;
	}
	public void setData(Calendar data) {
		this.data = data;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public String getBase64() {
		return base64;
	}
	public void setBase64(String base64) {
		this.base64 = base64;
	}
	
	
}
