package br.com.massuda.alexander.usuario.orm.modelo.usuario;

import java.util.Calendar;
import java.util.List;

import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Tabela;
import br.com.massuda.alexander.usuario.orm.modelo.Endereco;
import br.com.massuda.alexander.usuario.orm.modelo.Foto;
import br.com.massuda.alexander.usuario.orm.modelo.Perfil;
import br.com.massuda.alexander.usuario.orm.modelo.Pessoa;
import br.com.massuda.alexander.usuario.orm.modelo.RespostaUsuarioAutenticacao;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.academico.Escolaridade;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.profissional.Empresa;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.profissional.ExperienciaProfissional;

@Tabela(nome="tbl_usuario")
public class Usuario extends Pessoa {

	/**
	 * 
	 */
	private static final long serialVersionUID = 22664275874397043L;
	private String login;
	private String senha;
	private Calendar dataDeCriacao;
	private RespostaUsuarioAutenticacao status;
	private int contadorSenhaInvalida;
	private Perfil perfil;
	private List<Circulo> circulos; 
	private List<RedeSocial> redesSociais;
	private List<Endereco> enderecos;
	private Endereco naturalidade;
	private List<Telefone> telefones;
	private List<Escolaridade> historicoAcademico;
	private List<ExperienciaProfissional> historicoProfissional;
	private List<Foto> fotos;
	private List<Empresa> empresas;
	
	public String getLogin() {
		return login;
	}


	public void setLogin(String login) {
		this.login = login;
	}


	public String getSenha() {
		return senha;
	}


	public void setSenha(String senha) {
		this.senha = senha;
	}


	public Calendar getDataDeCriacao() {
		return dataDeCriacao;
	}


	public void setDataDeCriacao(Calendar dataDeCriacao) {
		this.dataDeCriacao = dataDeCriacao;
	}


	public RespostaUsuarioAutenticacao getStatus() {
		return status;
	}


	public void setStatus(RespostaUsuarioAutenticacao status) {
		this.status = status;
	}


	public int getContadorSenhaInvalida() {
		return contadorSenhaInvalida;
	}


	public void setContadorSenhaInvalida(int contadorSenhaInvalida) {
		this.contadorSenhaInvalida = contadorSenhaInvalida;
	}


	public Perfil getPerfil() {
		return perfil;
	}


	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}


	public List<Circulo> getCirculos() {
		return circulos;
	}


	public void setCirculos(List<Circulo> circulos) {
		this.circulos = circulos;
	}


	public List<RedeSocial> getRedesSociais() {
		return redesSociais;
	}


	public void setRedesSociais(List<RedeSocial> redesSociais) {
		this.redesSociais = redesSociais;
	}


	public List<Endereco> getEnderecos() {
		return enderecos;
	}


	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}


	public Endereco getNaturalidade() {
		return naturalidade;
	}


	public void setNaturalidade(Endereco naturalidade) {
		this.naturalidade = naturalidade;
	}


	public List<Telefone> getTelefones() {
		return telefones;
	}


	public void setTelefones(List<Telefone> telefones) {
		this.telefones = telefones;
	}


	public List<Escolaridade> getHistoricoAcademico() {
		return historicoAcademico;
	}


	public void setHistoricoAcademico(List<Escolaridade> historicoAcademico) {
		this.historicoAcademico = historicoAcademico;
	}


	public List<ExperienciaProfissional> getHistoricoProfissional() {
		return historicoProfissional;
	}


	public void setHistoricoProfissional(List<ExperienciaProfissional> historicoProfissional) {
		this.historicoProfissional = historicoProfissional;
	}


	public List<Foto> getFotos() {
		return fotos;
	}


	public void setFotos(List<Foto> fotos) {
		this.fotos = fotos;
	}


	public List<Empresa> getEmpresas() {
		return empresas;
	}


	public void setEmpresas(List<Empresa> empresas) {
		this.empresas = empresas;
	}


	public Usuario() {
		this.tipo = TipoPessoa.PESSOA_FISICA;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + contadorSenhaInvalida;
		result = prime * result + ((dataDeCriacao == null) ? 0 : dataDeCriacao.hashCode());
		result = prime * result + ((empresas == null) ? 0 : empresas.hashCode());
		result = prime * result + ((enderecos == null) ? 0 : enderecos.hashCode());
		result = prime * result + ((fotos == null) ? 0 : fotos.hashCode());
		result = prime * result + ((circulos == null) ? 0 : circulos.hashCode());
		result = prime * result + ((historicoAcademico == null) ? 0 : historicoAcademico.hashCode());
		result = prime * result + ((historicoProfissional == null) ? 0 : historicoProfissional.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((naturalidade == null) ? 0 : naturalidade.hashCode());
		result = prime * result + ((perfil == null) ? 0 : perfil.hashCode());
		result = prime * result + ((redesSociais == null) ? 0 : redesSociais.hashCode());
		result = prime * result + ((senha == null) ? 0 : senha.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((telefones == null) ? 0 : telefones.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Usuario)) {
			return false;
		}
		Usuario other = (Usuario) obj;
		if (contadorSenhaInvalida != other.contadorSenhaInvalida) {
			return false;
		}
		if (dataDeCriacao == null) {
			if (other.dataDeCriacao != null) {
				return false;
			}
		} else if (!dataDeCriacao.equals(other.dataDeCriacao)) {
			return false;
		}
		if (empresas == null) {
			if (other.empresas != null) {
				return false;
			}
		} else if (!empresas.equals(other.empresas)) {
			return false;
		}
		if (enderecos == null) {
			if (other.enderecos != null) {
				return false;
			}
		} else if (!enderecos.equals(other.enderecos)) {
			return false;
		}
		if (fotos == null) {
			if (other.fotos != null) {
				return false;
			}
		} else if (!fotos.equals(other.fotos)) {
			return false;
		}
		if (circulos == null) {
			if (other.circulos != null) {
				return false;
			}
		} else if (!circulos.equals(other.circulos)) {
			return false;
		}
		if (historicoAcademico == null) {
			if (other.historicoAcademico != null) {
				return false;
			}
		} else if (!historicoAcademico.equals(other.historicoAcademico)) {
			return false;
		}
		if (historicoProfissional == null) {
			if (other.historicoProfissional != null) {
				return false;
			}
		} else if (!historicoProfissional.equals(other.historicoProfissional)) {
			return false;
		}
		if (login == null) {
			if (other.login != null) {
				return false;
			}
		} else if (!login.equals(other.login)) {
			return false;
		}
		if (naturalidade == null) {
			if (other.naturalidade != null) {
				return false;
			}
		} else if (!naturalidade.equals(other.naturalidade)) {
			return false;
		}
		if (perfil == null) {
			if (other.perfil != null) {
				return false;
			}
		} else if (!perfil.equals(other.perfil)) {
			return false;
		}
		if (redesSociais == null) {
			if (other.redesSociais != null) {
				return false;
			}
		} else if (!redesSociais.equals(other.redesSociais)) {
			return false;
		}
		if (senha == null) {
			if (other.senha != null) {
				return false;
			}
		} else if (!senha.equals(other.senha)) {
			return false;
		}
		if (status != other.status) {
			return false;
		}
		if (telefones == null) {
			if (other.telefones != null) {
				return false;
			}
		} else if (!telefones.equals(other.telefones)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Usuario [login=").append(login).append(", senha=").append(senha).append(", dataDeCriacao=")
				.append(dataDeCriacao).append(", status=").append(status).append(", contadorSenhaInvalida=")
				.append(contadorSenhaInvalida).append(", perfil=").append(perfil).append(", circulos=").append(circulos)
				.append(", redesSociais=").append(redesSociais).append(", enderecos=").append(enderecos)
				.append(", naturalidade=").append(naturalidade).append(", telefones=").append(telefones)
				.append(", historicoAcademico=").append(historicoAcademico).append(", historicoProfissional=")
				.append(historicoProfissional).append(", fotos=").append(fotos).append(", empresas=").append(empresas)
				.append("]");
		return builder.toString();
	}


}
