package br.com.massuda.alexander.usuario.orm.modelo.usuario;

public enum Operadora {

	CLARO,
	NEXTEL,
	OI,
	TIM,
	VIVO
	
}
