package br.com.massuda.alexander.usuario.orm.modelo.usuario;

public enum NivelHierarquico {

	ADMINISTRADOR,
	COORDENADOR,
	DIRETOR,
	GERENTE,
	OPERADOR,
	PRESIDENTE,
	SUPERVISOR,
	USUARIO,
	VICE_PRESIDENTE;
	
}
