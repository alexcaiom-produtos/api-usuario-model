package br.com.massuda.alexander.usuario.orm.modelo.usuario.academico;

import java.util.List;

import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Tabela;
import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;
import br.com.massuda.alexander.usuario.orm.modelo.Endereco;

@Tabela(nome="tbl_instituicao_de_ensino")
public class InstituicaoDeEnsino extends EntidadeModelo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7703491977137530630L;
	private String nome;
	private Endereco endereco;

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endereco == null) ? 0 : endereco.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof InstituicaoDeEnsino)) {
			return false;
		}
		InstituicaoDeEnsino other = (InstituicaoDeEnsino) obj;
		if (endereco == null) {
			if (other.endereco != null) {
				return false;
			}
		} else if (!endereco.equals(other.endereco)) {
			return false;
		}
		if (nome == null) {
			if (other.nome != null) {
				return false;
			}
		} else if (!nome.equals(other.nome)) {
			return false;
		}
		return true;
	}
	@Override
	public String toString() {
		return "InstituicaoDeEnsino [nome=" + nome + ", endereco=" + endereco + "]";
	}
	
}
