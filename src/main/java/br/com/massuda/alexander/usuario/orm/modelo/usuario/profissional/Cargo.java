package br.com.massuda.alexander.usuario.orm.modelo.usuario.profissional;

import java.util.List;

import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Tabela;
import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Transiente;
import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;

@Tabela(nome="tbl_empresa_cargo")
public class Cargo extends EntidadeModelo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1593484455722114292L;
	private Empresa empresa;
	private String nome;
	private String responsabilidades;
	@Transiente
	private List<ExperienciaProfissional> profissionais;

	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getResponsabilidades() {
		return responsabilidades;
	}
	public void setResponsabilidades(String responsabilidades) {
		this.responsabilidades = responsabilidades;
	}
	public List<ExperienciaProfissional> getProfissionais() {
		return profissionais;
	}
	public void setProfissionais(List<ExperienciaProfissional> profissionais) {
		this.profissionais = profissionais;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((empresa == null) ? 0 : empresa.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((profissionais == null) ? 0 : profissionais.hashCode());
		result = prime * result + ((responsabilidades == null) ? 0 : responsabilidades.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cargo other = (Cargo) obj;
		if (empresa == null) {
			if (other.empresa != null)
				return false;
		} else if (!empresa.equals(other.empresa))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (profissionais == null) {
			if (other.profissionais != null)
				return false;
		} else if (!profissionais.equals(other.profissionais))
			return false;
		if (responsabilidades == null) {
			if (other.responsabilidades != null)
				return false;
		} else if (!responsabilidades.equals(other.responsabilidades))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Cargo [empresa=" + empresa + ", nome=" + nome + ", responsabilidades=" + responsabilidades
				+ ", profissionais=" + profissionais + ", id=" + id + "]";
	}
	
}
