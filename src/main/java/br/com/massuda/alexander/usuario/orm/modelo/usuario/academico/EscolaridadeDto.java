package br.com.massuda.alexander.usuario.orm.modelo.usuario.academico;

import java.util.Calendar;
import java.util.List;

import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Tabela;
import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Transiente;
import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;

public class EscolaridadeDto extends EntidadeModelo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5998035755196806966L;
	private String nomeDoCurso;
	private InstituicaoDeEnsino instituicao;
	private String inicio;
	private String fim;
	private AreaDeEstudo area;
	private List<AtividadeEscolar> atividades;
	private GrauDeEscolaridade escolaridade;
	@Transiente
	private List<Usuario> turma;
	
	public String getNomeDoCurso() {
		return nomeDoCurso;
	}
	public void setNomeDoCurso(String nomeDoCurso) {
		this.nomeDoCurso = nomeDoCurso;
	}
	public InstituicaoDeEnsino getInstituicao() {
		return instituicao;
	}
	public void setInstituicao(InstituicaoDeEnsino instituicao) {
		this.instituicao = instituicao;
	}
	public String getInicio() {
		return inicio;
	}
	public void setInicio(String inicio) {
		this.inicio = inicio;
	}
	public String getFim() {
		return fim;
	}
	public void setFim(String fim) {
		this.fim = fim;
	}
	public AreaDeEstudo getArea() {
		return area;
	}
	public void setArea(AreaDeEstudo area) {
		this.area = area;
	}
	public List<AtividadeEscolar> getAtividades() {
		return atividades;
	}
	public void setAtividades(List<AtividadeEscolar> atividades) {
		this.atividades = atividades;
	}
	public GrauDeEscolaridade getEscolaridade() {
		return escolaridade;
	}
	public void setEscolaridade(GrauDeEscolaridade escolaridade) {
		this.escolaridade = escolaridade;
	}
	public List<Usuario> getTurma() {
		return turma;
	}
	public void setTurma(List<Usuario> turma) {
		this.turma = turma;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((area == null) ? 0 : area.hashCode());
		result = prime * result + ((escolaridade == null) ? 0 : escolaridade.hashCode());
		result = prime * result + ((fim == null) ? 0 : fim.hashCode());
		result = prime * result + ((inicio == null) ? 0 : inicio.hashCode());
		result = prime * result + ((instituicao == null) ? 0 : instituicao.hashCode());
		result = prime * result + ((nomeDoCurso == null) ? 0 : nomeDoCurso.hashCode());
		result = prime * result + ((turma == null) ? 0 : turma.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EscolaridadeDto other = (EscolaridadeDto) obj;
		if (area == null) {
			if (other.area != null)
				return false;
		} else if (!area.equals(other.area))
			return false;
		if (escolaridade != other.escolaridade)
			return false;
		if (fim == null) {
			if (other.fim != null)
				return false;
		} else if (!fim.equals(other.fim))
			return false;
		if (inicio == null) {
			if (other.inicio != null)
				return false;
		} else if (!inicio.equals(other.inicio))
			return false;
		if (instituicao == null) {
			if (other.instituicao != null)
				return false;
		} else if (!instituicao.equals(other.instituicao))
			return false;
		if (nomeDoCurso == null) {
			if (other.nomeDoCurso != null)
				return false;
		} else if (!nomeDoCurso.equals(other.nomeDoCurso))
			return false;
		if (turma == null) {
			if (other.turma != null)
				return false;
		} else if (!turma.equals(other.turma))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Escolaridade [nomeDoCurso=" + nomeDoCurso + ", instituicao=" + instituicao + ", inicio=" + inicio
				+ ", fim=" + fim + ", area=" + area + ", escolaridade=" + escolaridade + ", turma=" + turma + ", id="
				+ id + "]";
	}
	
}
