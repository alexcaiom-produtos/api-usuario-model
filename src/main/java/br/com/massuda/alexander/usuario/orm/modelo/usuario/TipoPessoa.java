package br.com.massuda.alexander.usuario.orm.modelo.usuario;

public enum TipoPessoa {

	PESSOA_FISICA,
	PESSOA_JURIDICA
	
}
