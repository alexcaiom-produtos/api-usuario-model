//package br.com.massuda.alexander.usuario.orm.modelo.usuario;
//
//import java.util.List;
//
//import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Tabela;
//import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;
//
//@Tabela(nome = "tbl_grupo_circulos")
//public class GrupoCirculos extends EntidadeModelo{
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = -101228166954496093L;
//	private String titulo;
//	private String descricao;
//	private List<Circulo> circulosDoGrupo;
//	private List<Usuario> usuarios;
//	
//	public String getTitulo() {
//		return titulo;
//	}
//	public void setTitulo(String titulo) {
//		this.titulo = titulo;
//	}
//	public String getDescricao() {
//		return descricao;
//	}
//	public void setDescricao(String descricao) {
//		this.descricao = descricao;
//	}
//	public List<Circulo> getCirculosDoGrupo() {
//		return circulosDoGrupo;
//	}
//	public void setCirculosDoGrupo(List<Circulo> circulosDoGrupo) {
//		this.circulosDoGrupo = circulosDoGrupo;
//	}
//	public List<Usuario> getUsuarios() {
//		return usuarios;
//	}
//	public void setUsuarios(List<Usuario> usuarios) {
//		this.usuarios = usuarios;
//	}
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + ((circulosDoGrupo == null) ? 0 : circulosDoGrupo.hashCode());
//		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
//		result = prime * result + ((titulo == null) ? 0 : titulo.hashCode());
//		return result;
//	}
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		GrupoCirculos other = (GrupoCirculos) obj;
//		if (circulosDoGrupo == null) {
//			if (other.circulosDoGrupo != null)
//				return false;
//		} else if (!circulosDoGrupo.equals(other.circulosDoGrupo))
//			return false;
//		if (descricao == null) {
//			if (other.descricao != null)
//				return false;
//		} else if (!descricao.equals(other.descricao))
//			return false;
//		if (titulo == null) {
//			if (other.titulo != null)
//				return false;
//		} else if (!titulo.equals(other.titulo))
//			return false;
//		return true;
//	}
//	@Override
//	public String toString() {
//		return "GrupoDeCirculos [titulo=" + titulo + ", descricao=" + descricao + ", circulosDoGrupo=" + circulosDoGrupo
//				+ ", id=" + id + "]";
//	}
//}
