package br.com.massuda.alexander.usuario.orm.modelo.sistema.autorizacao;

import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Tabela;
import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;
import br.com.massuda.alexander.usuario.orm.modelo.sistema.Funcionalidade;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;

@Tabela(nome="tbl_permissao")
public class Permissao extends EntidadeModelo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5867448376068130476L;
	private Usuario usuario;
	private Funcionalidade funcionalidade;
	private Boolean escrita;
	private Boolean gravacao;
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Funcionalidade getFuncionalidade() {
		return funcionalidade;
	}
	public void setFuncionalidade(Funcionalidade funcionalidade) {
		this.funcionalidade = funcionalidade;
	}
	public Boolean getEscrita() {
		return escrita;
	}
	public void setEscrita(Boolean escrita) {
		this.escrita = escrita;
	}
	public Boolean getGravacao() {
		return gravacao;
	}
	public void setGravacao(Boolean gravacao) {
		this.gravacao = gravacao;
	}
	@Override
	public String toString() {
		return "Permissao [usuario=" + usuario + ", funcionalidade=" + funcionalidade + ", escrita=" + escrita
				+ ", gravacao=" + gravacao + ", id=" + id + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((escrita == null) ? 0 : escrita.hashCode());
		result = prime * result + ((funcionalidade == null) ? 0 : funcionalidade.hashCode());
		result = prime * result + ((gravacao == null) ? 0 : gravacao.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Permissao)) {
			return false;
		}
		Permissao other = (Permissao) obj;
		if (escrita == null) {
			if (other.escrita != null) {
				return false;
			}
		} else if (!escrita.equals(other.escrita)) {
			return false;
		}
		if (funcionalidade == null) {
			if (other.funcionalidade != null) {
				return false;
			}
		} else if (!funcionalidade.equals(other.funcionalidade)) {
			return false;
		}
		if (gravacao == null) {
			if (other.gravacao != null) {
				return false;
			}
		} else if (!gravacao.equals(other.gravacao)) {
			return false;
		}
		if (usuario == null) {
			if (other.usuario != null) {
				return false;
			}
		} else if (!usuario.equals(other.usuario)) {
			return false;
		}
		return true;
	}
	
	
}
