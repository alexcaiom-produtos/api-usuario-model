package br.com.massuda.alexander.usuario.orm.modelo.usuario.profissional;

import java.util.Calendar;
import java.util.List;

import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Tabela;
import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.UsuarioNivelVisualizacao;

@Tabela(nome="tbl_experiencia_profissional")
public class ExperienciaProfissional extends EntidadeModelo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5729426785366703554L;
	private List<Cargo> cargo;
	private Calendar inicio;
	private Calendar fim;
	private UsuarioNivelVisualizacao nivelAcesso;
	
	public List<Cargo> getCargo() {
		return cargo;
	}
	public void setCargo(List<Cargo> cargo) {
		this.cargo = cargo;
	}
	public Calendar getInicio() {
		return inicio;
	}
	public void setInicio(Calendar inicio) {
		this.inicio = inicio;
	}
	public Calendar getFim() {
		return fim;
	}
	public void setFim(Calendar fim) {
		this.fim = fim;
	}
	public UsuarioNivelVisualizacao getNivelAcesso() {
		return nivelAcesso;
	}
	public void setNivelAcesso(UsuarioNivelVisualizacao nivelAcesso) {
		this.nivelAcesso = nivelAcesso;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cargo == null) ? 0 : cargo.hashCode());
		result = prime * result + ((fim == null) ? 0 : fim.hashCode());
		result = prime * result + ((inicio == null) ? 0 : inicio.hashCode());
		result = prime * result + ((nivelAcesso == null) ? 0 : nivelAcesso.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExperienciaProfissional other = (ExperienciaProfissional) obj;
		if (cargo == null) {
			if (other.cargo != null)
				return false;
		} else if (!cargo.equals(other.cargo))
			return false;
		if (fim == null) {
			if (other.fim != null)
				return false;
		} else if (!fim.equals(other.fim))
			return false;
		if (inicio == null) {
			if (other.inicio != null)
				return false;
		} else if (!inicio.equals(other.inicio))
			return false;
		if (nivelAcesso != other.nivelAcesso)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ExperienciaProfissional [cargo=" + cargo + ", inicio=" + inicio + ", fim=" + fim + ", nivelAcesso="
				+ nivelAcesso + ", id=" + id + "]";
	} 
	
	
}
