package br.com.massuda.alexander.usuario.orm.modelo.usuario;

import java.util.List;

import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Tabela;
import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Transiente;
import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.profissional.Empresa;

@Tabela(nome="tbl_telefone")
public class Telefone extends EntidadeModelo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2699976279262583981L;
	private Integer ddd;
	private Integer ddi;
	private String numero;
	private TipoTelefone tipo;
	private Operadora operadora;
	@Transiente
	private List<Usuario> usuarios;
	@Transiente
	private List<Empresa> empresas;
	
	public Integer getDdd() {
		return ddd;
	}
	public void setDdd(Integer ddd) {
		this.ddd = ddd;
	}
	public Integer getDdi() {
		return ddi;
	}
	public void setDdi(Integer ddi) {
		this.ddi = ddi;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public TipoTelefone getTipo() {
		return tipo;
	}
	public void setTipo(TipoTelefone tipo) {
		this.tipo = tipo;
	}
	public Operadora getOperadora() {
		return operadora;
	}
	public void setOperadora(Operadora operadora) {
		this.operadora = operadora;
	}
	public List<Usuario> getUsuarios() {
		return usuarios;
	}
	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
	public List<Empresa> getEmpresas() {
		return empresas;
	}
	public void setEmpresas(List<Empresa> empresas) {
		this.empresas = empresas;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ddd == null) ? 0 : ddd.hashCode());
		result = prime * result + ((ddi == null) ? 0 : ddi.hashCode());
		result = prime * result + ((empresas == null) ? 0 : empresas.hashCode());
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		result = prime * result + ((operadora == null) ? 0 : operadora.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		result = prime * result + ((usuarios == null) ? 0 : usuarios.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Telefone)) {
			return false;
		}
		Telefone other = (Telefone) obj;
		if (ddd == null) {
			if (other.ddd != null) {
				return false;
			}
		} else if (!ddd.equals(other.ddd)) {
			return false;
		}
		if (ddi == null) {
			if (other.ddi != null) {
				return false;
			}
		} else if (!ddi.equals(other.ddi)) {
			return false;
		}
		if (empresas == null) {
			if (other.empresas != null) {
				return false;
			}
		} else if (!empresas.equals(other.empresas)) {
			return false;
		}
		if (numero == null) {
			if (other.numero != null) {
				return false;
			}
		} else if (!numero.equals(other.numero)) {
			return false;
		}
		if (operadora != other.operadora) {
			return false;
		}
		if (tipo != other.tipo) {
			return false;
		}
		if (usuarios == null) {
			if (other.usuarios != null) {
				return false;
			}
		} else if (!usuarios.equals(other.usuarios)) {
			return false;
		}
		return true;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Telefone [ddd=").append(ddd).append(", ddi=").append(ddi).append(", numero=").append(numero)
				.append(", tipo=").append(tipo).append(", operadora=").append(operadora).append(", usuarios=")
				.append(usuarios).append(", empresas=").append(empresas).append(", id=").append(id).append("]");
		return builder.toString();
	}
}
