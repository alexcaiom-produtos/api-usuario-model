package br.com.massuda.alexander.usuario.orm.modelo.usuario.profissional;

import java.util.List;

import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Tabela;
import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Transiente;
import br.com.massuda.alexander.usuario.orm.modelo.Endereco;
import br.com.massuda.alexander.usuario.orm.modelo.Pessoa;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Circulo;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Telefone;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.TipoPessoa;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;

@Tabela(nome="tbl_empresa")
public class Empresa extends Pessoa {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7435684190754485830L;
	private List<Segmento> segmentos;
	private List<Endereco> enderecos;
	private List<Telefone> telefones;
	private List<Circulo> circulos;
	@Transiente
	private List<Usuario> usuarios;

	
	public Empresa() {
		this.tipo = TipoPessoa.PESSOA_JURIDICA;
	}


	public List<Segmento> getSegmentos() {
		return segmentos;
	}


	public void setSegmentos(List<Segmento> segmentos) {
		this.segmentos = segmentos;
	}


	public List<Endereco> getEnderecos() {
		return enderecos;
	}


	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}


	public List<Telefone> getTelefones() {
		return telefones;
	}


	public void setTelefones(List<Telefone> telefones) {
		this.telefones = telefones;
	}


	public List<Circulo> getCirculos() {
		return circulos;
	}


	public void setCirculos(List<Circulo> circulos) {
		this.circulos = circulos;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((circulos == null) ? 0 : circulos.hashCode());
		result = prime * result + ((enderecos == null) ? 0 : enderecos.hashCode());
		result = prime * result + ((segmentos == null) ? 0 : segmentos.hashCode());
		result = prime * result + ((telefones == null) ? 0 : telefones.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Empresa)) {
			return false;
		}
		Empresa other = (Empresa) obj;
		if (circulos == null) {
			if (other.circulos != null) {
				return false;
			}
		} else if (!circulos.equals(other.circulos)) {
			return false;
		}
		if (enderecos == null) {
			if (other.enderecos != null) {
				return false;
			}
		} else if (!enderecos.equals(other.enderecos)) {
			return false;
		}
		if (segmentos == null) {
			if (other.segmentos != null) {
				return false;
			}
		} else if (!segmentos.equals(other.segmentos)) {
			return false;
		}
		if (telefones == null) {
			if (other.telefones != null) {
				return false;
			}
		} else if (!telefones.equals(other.telefones)) {
			return false;
		}
		return true;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Empresa [segmentos=").append(segmentos).append(", enderecos=").append(enderecos)
				.append(", telefones=").append(telefones).append(", circulos=").append(circulos).append(", nome=")
				.append(nome).append(", dataDeNascimento=").append(dataDeNascimento).append(", email=").append(email)
				.append(", identificacao=").append(identificacao).append(", tipo=").append(tipo).append(", id=")
				.append(id).append("]");
		return builder.toString();
	}


	public List<Usuario> getUsuarios() {
		return usuarios;
	}


	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}


}
