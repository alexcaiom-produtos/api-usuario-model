package br.com.massuda.alexander.usuario.orm.modelo;

import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Tabela;
import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.NivelHierarquico;

@Tabela(nome="tbl_perfil")
public class Perfil extends EntidadeModelo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7145842631171233551L;
	private String nome;
	private NivelHierarquico nivel;
	
	public Perfil() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param nome
	 * @param nivel
	 */
	public Perfil(String nome, NivelHierarquico nivel) {
		this.nome = nome;
		this.nivel = nivel;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public NivelHierarquico getNivel() {
		return nivel;
	}
	public void setNivel(NivelHierarquico nivel) {
		this.nivel = nivel;
	}

	
	
	@Override
	public String toString() {
		return "Perfil [nome=" + nome + ", nivel=" + nivel + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nivel == null) ? 0 : nivel.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Perfil))
			return false;
		Perfil other = (Perfil) obj;
		if (id != other.id)
			return false;
		if (nivel != other.nivel)
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}
	
}
