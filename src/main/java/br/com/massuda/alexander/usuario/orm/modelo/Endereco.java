package br.com.massuda.alexander.usuario.orm.modelo;

import java.util.List;

import br.com.massuda.alexander.persistencia.jdbc.anotacoes.ChaveEstrangeira;
import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Tabela;
import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Transiente;
import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.CoordenadasGeograficas;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.TipoEndereco;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.academico.InstituicaoDeEnsino;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.profissional.Empresa;

@Tabela(nome="tbl_endereco")
public class Endereco extends EntidadeModelo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1726229667140565481L;
	@ChaveEstrangeira(nome = "coordenadas_id")
	private CoordenadasGeograficas coordenadasGeograficas;
	private Integer numero;
	private String complemento;
	private TipoEndereco tipo;
	@Transiente
	private List<Usuario> moradores;
	@Transiente
	private List<Empresa> empresas;
	@Transiente
	private InstituicaoDeEnsino instituicoes;
	
	public CoordenadasGeograficas getCoordenadasGeograficas() {
		return coordenadasGeograficas;
	}
	public void setCoordenadasGeograficas(CoordenadasGeograficas coordenadasGeograficas) {
		this.coordenadasGeograficas = coordenadasGeograficas;
	}
	public Integer getNumero() {
		return numero;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public TipoEndereco getTipo() {
		return tipo;
	}
	public void setTipo(TipoEndereco tipo) {
		this.tipo = tipo;
	}
	public List<Usuario> getMoradores() {
		return moradores;
	}
	public void setMoradores(List<Usuario> moradores) {
		this.moradores = moradores;
	}
	public List<Empresa> getEmpresas() {
		return empresas;
	}
	public void setEmpresas(List<Empresa> empresas) {
		this.empresas = empresas;
	}
	public InstituicaoDeEnsino getInstituicoes() {
		return instituicoes;
	}
	public void setInstituicoes(InstituicaoDeEnsino instituicoes) {
		this.instituicoes = instituicoes;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((complemento == null) ? 0 : complemento.hashCode());
		result = prime * result + ((coordenadasGeograficas == null) ? 0 : coordenadasGeograficas.hashCode());
		result = prime * result + ((empresas == null) ? 0 : empresas.hashCode());
		result = prime * result + ((instituicoes == null) ? 0 : instituicoes.hashCode());
		result = prime * result + ((moradores == null) ? 0 : moradores.hashCode());
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Endereco)) {
			return false;
		}
		Endereco other = (Endereco) obj;
		if (complemento == null) {
			if (other.complemento != null) {
				return false;
			}
		} else if (!complemento.equals(other.complemento)) {
			return false;
		}
		if (coordenadasGeograficas == null) {
			if (other.coordenadasGeograficas != null) {
				return false;
			}
		} else if (!coordenadasGeograficas.equals(other.coordenadasGeograficas)) {
			return false;
		}
		if (empresas == null) {
			if (other.empresas != null) {
				return false;
			}
		} else if (!empresas.equals(other.empresas)) {
			return false;
		}
		if (instituicoes == null) {
			if (other.instituicoes != null) {
				return false;
			}
		} else if (!instituicoes.equals(other.instituicoes)) {
			return false;
		}
		if (moradores == null) {
			if (other.moradores != null) {
				return false;
			}
		} else if (!moradores.equals(other.moradores)) {
			return false;
		}
		if (numero == null) {
			if (other.numero != null) {
				return false;
			}
		} else if (!numero.equals(other.numero)) {
			return false;
		}
		if (tipo != other.tipo) {
			return false;
		}
		return true;
	}
	@Override
	public String toString() {
		return "Endereco [coordenadasGeograficas=" + coordenadasGeograficas + ", numero=" + numero + ", complemento="
				+ complemento + ", tipo=" + tipo + ", moradores=" + moradores + ", empresas=" + empresas
				+ ", instituicoes=" + instituicoes + "]";
	}
	
	
}
