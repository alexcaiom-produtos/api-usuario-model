package br.com.massuda.alexander.usuario.orm.modelo.usuario.academico;

import java.util.List;

import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Transiente;
import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;

public class AtividadeEscolar extends EntidadeModelo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2484831222821195916L;
	private String nome;
	private String descricao;
	@Transiente
	private List<Escolaridade> escolaridades;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public List<Escolaridade> getEscolaridades() {
		return escolaridades;
	}
	public void setEscolaridades(List<Escolaridade> escolaridades) {
		this.escolaridades = escolaridades;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((escolaridades == null) ? 0 : escolaridades.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof AtividadeEscolar)) {
			return false;
		}
		AtividadeEscolar other = (AtividadeEscolar) obj;
		if (descricao == null) {
			if (other.descricao != null) {
				return false;
			}
		} else if (!descricao.equals(other.descricao)) {
			return false;
		}
		if (escolaridades == null) {
			if (other.escolaridades != null) {
				return false;
			}
		} else if (!escolaridades.equals(other.escolaridades)) {
			return false;
		}
		if (nome == null) {
			if (other.nome != null) {
				return false;
			}
		} else if (!nome.equals(other.nome)) {
			return false;
		}
		return true;
	}
	@Override
	public String toString() {
		return "AtividadeEscolar [nome=" + nome + ", descricao=" + descricao + ", escolaridades=" + escolaridades
				+ ", id=" + id + "]";
	}
	
	
	
}
