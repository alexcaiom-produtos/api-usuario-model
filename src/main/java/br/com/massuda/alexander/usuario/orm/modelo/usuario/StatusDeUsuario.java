package br.com.massuda.alexander.usuario.orm.modelo.usuario;

public enum StatusDeUsuario {

	OK,
	BLOQUEADO,
	BLOQUEADO_POR_SENHA_INVALIDA;
	
}
