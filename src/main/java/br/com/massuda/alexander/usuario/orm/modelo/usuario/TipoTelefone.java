package br.com.massuda.alexander.usuario.orm.modelo.usuario;

public enum TipoTelefone {

	RESIDENCIAL,
	COMERCIAL,
	CELULAR
	
}
